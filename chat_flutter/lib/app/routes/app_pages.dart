import 'package:chat_flutter/app/modules/home/views/chat_view.dart';
import 'package:get/get.dart';

import 'package:chat_flutter/app/modules/home/bindings/auth_binding.dart';
import 'package:chat_flutter/app/modules/home/views/auth_view.dart';
import 'package:chat_flutter/app/modules/home/bindings/home_binding.dart';
import 'package:chat_flutter/app/modules/home/views/home_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.AUTH;

  static final routes = [
    GetPage(
        name: _Paths.HOME,
        page: () => HomeView(),
        binding: HomeBinding(),
        children: [GetPage(name: _Paths.CHAT, page: () => ChatView())]),
    GetPage(
      name: _Paths.AUTH,
      page: () => AuthView(),
      binding: LoginBinding(),
    ),
  ];
}
