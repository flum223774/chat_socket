import 'package:chat_flutter/app/data/models/chat_model.dart';
import 'package:get/get.dart';

class AuthController extends GetxController {
  static AuthController get to => Get.find();

  List<ChatModel> allUser = [
    ChatModel(
      name: "Nam",
      isGroup: false,
      currentMessage: "Hi Everyone",
      time: "4:00",
      icon: "person.svg",
      id: 1,
    ),
    ChatModel(
      name: "Liên",
      isGroup: false,
      currentMessage: "Hi Kishor",
      time: "13:00",
      icon: "person.svg",
      id: 2,
    ),

    ChatModel(
      name: "Hoa ",
      isGroup: false,
      currentMessage: "Hi Dev Stack",
      time: "8:00",
      icon: "person.svg",
      id: 3,
    ),

    ChatModel(
      name: "Kim",
      isGroup: false,
      currentMessage: "Hi Dev Stack",
      time: "2:00",
      icon: "person.svg",
      id: 4,
    ),
    // ChatModel(
    //   name: "NodeJs Group",
    //   isGroup: true,
    //   currentMessage: "New NodejS Post",
    //   time: "2:00",
    //   icon: "group.svg",
    // ),
  ];

  List<ChatModel> get allChatUser =>
      allUser.where((element) => element.id != _userModel.value!.id).toList();

  Rx<ChatModel?> _userModel = Rx(null);

  ChatModel? get userModel => _userModel.value;

  setUser(ChatModel userModel) {
    _userModel(userModel);
  }
}
