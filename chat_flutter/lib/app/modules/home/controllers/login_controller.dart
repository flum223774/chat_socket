import 'package:chat_flutter/app/data/models/chat_model.dart';
import 'package:chat_flutter/app/modules/home/controllers/auth_controller.dart';
import 'package:chat_flutter/app/modules/home/provider/chat_provider.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  AuthController _authController = AuthController.to;
  ChatProvider _chatProvider = ChatProvider.to;

  @override
  void onInit() {
    super.onInit();
  }

  Future<void> login(ChatModel sourceChat) async {
    _chatProvider.socketClient.emit('signin', sourceChat.id);
    _authController.setUser(sourceChat);
    Get.toNamed('/home');
  }
}
