import 'package:chat_flutter/app/data/models/chat_model.dart';
import 'package:chat_flutter/app/data/models/message_model.dart';
import 'package:chat_flutter/app/modules/home/controllers/auth_controller.dart';
import 'package:chat_flutter/app/modules/home/provider/chat_provider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  AuthController _authController = AuthController.to;
  TextEditingController msgController = TextEditingController();
  ChatProvider _provider = ChatProvider.to;
  RxList<MessageModel> _messages = RxList<MessageModel>([]);

  List<MessageModel> get messages => _messages;

  Rx<ChatModel?> _targetUser = Rx<ChatModel?>(null);

  ChatModel? get targetUser => _targetUser.value;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    _provider.socketClient.on("message", (data) {
      print('listen on $data');
      setMessage("destination", data["message"]);
    });
  }

  @override
  void onClose() {}

  void sendMessage(String message, int sourceId, int targetId) {
    setMessage("source", message);
    _provider.socketClient.emit("message",
        {"message": message, "sourceId": sourceId, "targetId": targetId});
  }

  void setMessage(String type, String message) {
    MessageModel messageModel = MessageModel(
        type: type,
        message: message,
        time: DateTime.now().toString().substring(10, 16));
    print(messages);

    _messages.add(messageModel);
  }

  void setTargetUser(ChatModel userModel) {
    _targetUser(userModel);
    Get.toNamed('/home/chat');
  }

  submit() {
    sendMessage(
        msgController.text, _authController.userModel!.id, targetUser!.id);
    msgController.text = '';
  }
}
