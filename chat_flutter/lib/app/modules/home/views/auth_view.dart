import 'package:chat_flutter/app/data/models/chat_model.dart';
import 'package:chat_flutter/app/modules/home/controllers/auth_controller.dart';
import 'package:chat_flutter/app/modules/home/controllers/login_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:get/get.dart';

class AuthView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Chattr.io',
            style: Get.textTheme.headline3!.copyWith(color: Colors.black),
          ),
          SizedBox(
            height: 16,
          ),
          Text('Chọn 1 trong các thành viên sau để đăng nhập'),
          SizedBox(height: 20),
          ListView(
            padding: EdgeInsets.symmetric(horizontal: 10),
            shrinkWrap: true,
            children: List.generate(
                AuthController.to.allUser.length,
                (index) =>
                    _buildItem(AuthController.to.allUser.elementAt(index))),
          )
        ],
      ),
    ));
  }

  Widget _buildItem(ChatModel item) {
    return Card(
      child: ListTile(
        title: Text('${item.name}'),
        onTap: () => controller.login(item),
      ),
    );
  }
}
