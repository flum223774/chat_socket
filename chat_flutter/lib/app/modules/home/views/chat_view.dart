import 'package:chat_flutter/app/modules/home/controllers/auth_controller.dart';
import 'package:chat_flutter/app/modules/home/controllers/home_controller.dart';
import 'package:chat_flutter/app/modules/home/widgets/own_messgae_card.dart';
import 'package:chat_flutter/app/modules/home/widgets/reply_card.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ChatView extends GetView<HomeController> {
  const ChatView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('${controller.targetUser!.name}'),
        ),
        body: Stack(children: [
          Column(children: [
            Expanded(
                child: Obx(()=>ListView.builder(
                    shrinkWrap: true,
                    itemCount: controller.messages.length + 1,
                    itemBuilder: (context, index) {
                      if (index == controller.messages.length) {
                        return Container(
                          height: 70,
                        );
                      }
                      if (controller.messages[index].type == "source") {
                        return OwnMessageCard(
                          message: controller.messages[index].message,
                          time: controller.messages[index].time,
                        );
                      } else {
                        return ReplyCard(
                          message: controller.messages[index].message,
                          time: controller.messages[index].time,
                        );
                      }
                    })))
          ]),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  height: 70,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            Container(
                                width: MediaQuery.of(context).size.width - 60,
                                child: Card(
                                    margin: EdgeInsets.only(
                                        left: 8, right: 2, bottom: 8),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(25),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 16.0),
                                      child: TextFormField(
                                        controller: controller.msgController,
                                        textAlignVertical:
                                            TextAlignVertical.center,
                                        keyboardType: TextInputType.multiline,
                                        maxLines: 5,
                                        minLines: 1,
                                        onChanged: (value) {},
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: "Type a message",
                                          hintStyle:
                                              TextStyle(color: Colors.grey),
                                        ),
                                      ),
                                    ))),
                            Padding(
                              padding: const EdgeInsets.only(
                                bottom: 8,
                                right: 2,
                                left: 2,
                              ),
                              child: CircleAvatar(
                                radius: 20,
                                backgroundColor: Color(0xFF128C7E),
                                child: IconButton(
                                  icon: Icon(
                                    Icons.send,
                                    color: Colors.white,
                                  ),
                                  onPressed: controller.submit,
                                ),
                              ),
                            )
                          ],
                        )
                      ])))
        ]));
  }
}
