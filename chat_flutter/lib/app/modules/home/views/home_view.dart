import 'package:chat_flutter/app/data/models/chat_model.dart';
import 'package:chat_flutter/app/modules/home/controllers/auth_controller.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Chọn người chat'),
        ),
        body: ListView(
          padding: EdgeInsets.all(16),
          children: List.generate(
              AuthController.to.allUser.length,
              (index) =>
                  _buildItem(AuthController.to.allUser.elementAt(index))),
        ));
  }

  Widget _buildItem(ChatModel item) {
    return Card(
      child: ListTile(
        title: Text('${item.name}'),
        onTap: () {
          if (item.id != AuthController.to.userModel!.id) {
            controller.setTargetUser(item);
          } else {
            Get.snackbar("Lỗi", "Bạn không thể chat với chinh bạn đươc ",
                snackPosition: SnackPosition.BOTTOM,
                backgroundColor: Colors.grey);
          }
        },
        trailing: item.id != AuthController.to.userModel!.id
            ? null
            : Icon(Icons.lock),
      ),
    );
  }
}
