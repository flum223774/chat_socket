import 'package:get/get.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

class ChatProvider extends GetConnect {
  static ChatProvider get to => Get.find();
  late IO.Socket socketClient;

  @override
  void onInit() {
    super.onInit();
    connect();
  }

  void connect() {
    // MessageModel messageModel = MessageModel(sourceId: widget.sourceChat.id.toString(),targetId: );
    socketClient = IO.io("http://192.168.1.66:5000", <String, dynamic>{
      "transports": ["websocket"],
      "autoConnect": false,
    });
    socketClient.connect();
    socketClient.onConnect((data) {
      print("Connected");
      // socketClient.on("message", (msg) {
      //   print('Msg $msg');
      //   // setMessage("destination", msg["message"]);
      //   // _scrollController.animateTo(_scrollController.position.maxScrollExtent,
      //   //     duration: Duration(milliseconds: 300), curve: Curves.easeOut);
      // });
    });
    socketClient.onError((data) => print('Error $data'));
    print('Socket connected ${socketClient.connected}');
  }
}
