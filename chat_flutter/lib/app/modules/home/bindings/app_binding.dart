import 'package:chat_flutter/app/modules/home/controllers/auth_controller.dart';
import 'package:chat_flutter/app/modules/home/provider/chat_provider.dart';
import 'package:get/get.dart';

class AppBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(AuthController());
    Get.put(ChatProvider());
  }
}
